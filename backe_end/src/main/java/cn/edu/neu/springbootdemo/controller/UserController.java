package cn.edu.neu.springbootdemo.controller;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.model.Result;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Controller
@RequestMapping()
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/login")
    @ResponseBody
    public Result login(@RequestBody User user){
        Result result = new Result();
        User dbUser=userService.existsUser(user);

        if (dbUser.getPassword().equals(user.getPassword())) {
            result.setCode(1);
            result.setMsg("登录成功");
            return result;
        } else {
            result.setCode(-1);
            result.setMsg("登录失败");
            return result;
        }
    }

    @PostMapping("/getAllUser")
    @ResponseBody
    public List<User> getUserList(){
        List<User> list=userService.getUserList();
        return list;
    }

//    @RequestMapping("/getUser")
//    public Map<String,User> getUser(Integer userid){
//        Map<String,User> map=new HashMap<>();
//        map.put("result",userService.findById(userid));
//        return map;
//    }
//
//    @RequestMapping("/addUser")
//    public Map<String,Object> addUser(User user){
//        Map<String,Object> map=new HashMap<>();
//        int i=userService.addUser(user);
//        if(i==1){
//            map.put("result","yes");
//            map.put("msg","恭喜你,注册成功！");
//        }else{
//            map.put("result","no");
//            map.put("msg","格式有误，注册失败！");
//        }
//        System.out.println(map);
//        return map;
//
//    }
//
//    @RequestMapping("/updateUser")
//    public Map<String,Object> updateUser(User user){
//        Map<String,Object> map=new HashMap<>();
//        int i=userService.updateUser(user);
//        if(i==1){
//            map.put("result","yes");
//            map.put("msg","恭喜你,修改成功！");
//        }else {
//            map.put("result","no");
//            map.put("msg","修改失败！");
//        }
//        return  map;
//    }
//
//    @RequestMapping("/deleteUser")
//    public Map<String,Object> deleteUser(Integer userid){
//        Map<String,Object> map=new HashMap<>();
//        int i=userService.deleteUser(userid);
//        if(i==1){
//            map.put("result","yes");
//            map.put("msg","恭喜你,删除成功！");
//        }else{
//            map.put("result","no");
//            map.put("msg","删除失败！");
//        }
//        return map;
//    }
//
//    @RequestMapping("/checkName")
//    public Map<String, String> checkName(String username){
//        int i =userService.checkNanme(username);
//        Map<String, String> m =new HashMap<String, String>();
//        if(i==1){
//            m.put("result","no");
//            m.put("msg","对不起，用户名已经存在");
//        }else{
//            m.put("result","yes");
//            m.put("msg","很棒的名字");
//        }
//        System.out.println("checkUsername:"+m);
//        return m;
//    }

}
