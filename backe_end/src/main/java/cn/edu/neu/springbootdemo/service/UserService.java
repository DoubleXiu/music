package cn.edu.neu.springbootdemo.service;

import cn.edu.neu.springbootdemo.model.User;

import java.util.List;

public interface UserService {

    public User existsUser(User user);
    public List<User> getUserList();
    public User findById(int userid);
    public int addUser(User user);
    public int updateUser(User user);
    public int deleteUser(Integer userid);
    public int checkNanme(String username);
}
