package cn.edu.neu.springbootdemo.service.impl;

import cn.edu.neu.springbootdemo.mapper.UserMapper;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserMapper userMapper;


    @Override
    public User existsUser(User user) {

        return userMapper.existsUser(user);
    }

    @Override
    public List<User> getUserList() {
        return userMapper.getUserList();
    }
    @Override
    public User findById(int userid) {

        return userMapper.findById(userid);
    }

    @Override
    public  int addUser(User user) {
        try{
            userMapper.addUser(user);
            return 1;
        }catch (Exception e){
            e.printStackTrace();
            return 2;
        }
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateUser(user);
    }

    @Override
    public int deleteUser(Integer userid) {
        return userMapper.deleteUser(userid);
    }

    @Override
    public int checkNanme(String username) {
        System.out.println(username);
        int i=userMapper.checkNanme(username);
        return i;
    }
}
